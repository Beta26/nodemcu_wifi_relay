# Wifi controlled relay with a Node MCU

### Description 

This is a project to controll a device (up to 250V) with a relay connected to a Node MCU board (ESP8266). This project is based on the code of [this](https://randomnerdtutorials.com/esp8266-relay-module-ac-web-server/) website. 

### Programming the ESP8266 board 

To programm the Node MCU with Arduino IDE 
1. Open the file *Node_MCU_Relay.ino* 
2. Install ESP Boards (http://arduino.esp8266.com/stable/package_esp8266com_index.json)
3. Tools> Board: "NodeMCU 1.0"
4. Change *line 13*: number of relays 
5. Change *line 16*: GPIOs
6. Change *line 19/20*: wlan and password 
7. Change *line 46*: Name and layout
8. Change *line 65*: Button name 

### Wiring 
Connect the relay to 5V, GND and an OUT Pin. 

#### Next Step

Communicate between boards to controll them using one ui [slave/master communication](https://randomnerdtutorials.com/esp-now-many-to-one-esp8266-nodemcu/)